# microapp-coding-exercise: npm-grapher

### Guidelines:
* This task is to completed using javascript and nodejs v8.x.
* You have 1 week to submit your response, and and it shouldn't take you more than 4 hours to complete. It won't be held against you if you don't complete it, but be prepared to discuss the code you do have in depth.
* You can use any libraries at all to complete the task except for any that basically do the exact thing this task is asking for.
* You may use es5/es2015/whatever javascript syntax (we have provided the babel config we use internally, but you may feel free to modify it/ignore it)

### Task:
Create a CLI nodejs app with the following command line interface:

```Shell
$ npm-dep-viz <npm-package-name>
```

The app should generate a tree visualization of all of the dependencies of the specified npm package.

That connects to the npm registry directly (no parsing npm shell commands), and creates a tree visualization of the dependencies of that package.

#### Design Decisions

The following decisions are left up to you, and have no "correct" answer we are looking for. We are more interested in seeing how you approach the problems.

* How to get the info on the npm package / dependencies
* The format/style of the tree visualization (can be output to the terminal, as file(s), or whatever you can think of)
* Testing: your time is limited, you must choose what are the important parts of your app to test (see below)

#### Testing

Since time is so limited, we don't expect you to have 100% code coverage. That being said, we'd like to see how you approach testing, so try to have at least 1 test, and some comments or stubbed out tests descibing what you think would be important to test in this app.

### Bonus points:
The following features are totally optional, but are things we thought would be nice to have if you have time.

* Extra cli options (ex: `--devDependencies` to output only the devDependencies of the project, or `--peerDependencies` to output the peerDependency tree)
* Ability to specify the version of a package to visualize
* `--at-date=2016-09-09T17:31:43.609Z` option to graph what was the current version at a given date
* Option for machine readable output (ex: `--json`)
